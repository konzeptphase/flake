<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use RedBeanPHP\Finder;

//load orm
require_once dirname(__FILE__) . '/orm/rb.php';

//override database credentials
if (isset($GLOBALS['instance_database_connection_string']) && $GLOBALS['instance_database_connection_string'])
    $GLOBALS['database_connection_string'] = $GLOBALS['instance_database_connection_string'];

//setup database
$dbHost = explode(':', $GLOBALS['database_connection_string'][1]);
R::setup($GLOBALS['database_connection_string'][0] . ':host=' . $dbHost[0] . (isset($dbHost[1]) && $dbHost[1] ? ';port=' . $dbHost[1] : '') . ';dbname=' . $GLOBALS['database_connection_string'][2], $GLOBALS['database_connection_string'][3], $GLOBALS['database_connection_string'][4], (isset($GLOBALS['database_connection_string'][5]) && $GLOBALS['database_connection_string'][5] ? true : false));

//load core classes
require_once(dirname(__FILE__) . '/core/flake.base.php');
require_once(dirname(__FILE__) . '/core/flake.session.php');
require_once(dirname(__FILE__) . '/core/flake.session.databasehandler.php');
require_once(dirname(__FILE__) . '/core/flake.setting.php');
require_once(dirname(__FILE__) . '/core/flake.security.php');
require_once(dirname(__FILE__) . '/core/flake.dev.php');
require_once(dirname(__FILE__) . '/core/flake.file.php');
require_once(dirname(__FILE__) . '/core/flake.cache.php');
require_once(dirname(__FILE__) . '/core/flake.cookie.php');
require_once(dirname(__FILE__) . '/core/flake.auth.php');
require_once(dirname(__FILE__) . '/core/flake.acl.php');
require_once(dirname(__FILE__) . '/core/flake.router.php');
require_once(dirname(__FILE__) . '/core/flake.netservice.php');
require_once(dirname(__FILE__) . '/core/flake.orm.php');
require_once(dirname(__FILE__) . '/core/flake.autoloader.php');
require_once(dirname(__FILE__) . '/core/flake.dataservice.php');
require_once(dirname(__FILE__) . '/core/flake.administration.php');
require_once(dirname(__FILE__) . '/core/flake.entity.php');
require_once(dirname(__FILE__) . '/core/flake.helper.php');
require_once(dirname(__FILE__) . '/core/flake.log.php');

//composer autoloader
require_once dirname(__FILE__) . '/vendor/autoload.php';

//inject all settings
flakeSetting::injectAll();

//maintenance mode
if (flakeSession::getSession('maintenance'))
    flakeAdministration::maintenance();
