<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeEntity
{
    public static function getEntityByName($entityName)
    {
        return R::findOne('flakeentity', 'name = ?', [$entityName]);
    }

    public static function getGrids($entityName)
    {
        $entity = self::getEntityByName($entityName);
        return R::findAll('flakegrid', 'entity_id = ? ORDER BY sorting ASC', [$entity->id]);
    }

    public static function getAttributes($grid)
    {
        return R::findAll('flakeattribute', 'grid_id = ? ORDER BY sorting ASC', [$grid->id]);
    }
    
    public static function save()
    {
        $entity = self::getEntityByName($_POST['entityType']);
        $attributes = R::findAll('flakeattribute', 'entity_id = ? ORDER BY sorting ASC', [$entity->id]);

        if ($_POST['entityId'] == 'new')
            $model = R::dispense($_POST['entityType']);
        else
            $model = R::findOne($_POST['entityType'], 'id = ?', [$_POST['entityId']]);

        foreach ($attributes as $oneAttribute) {
            $model->{$oneAttribute->name} = $_POST['flakeEntity-' . $oneAttribute->id];
        }
        R::store($model);
    }
}
