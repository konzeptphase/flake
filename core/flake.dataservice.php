<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeDataservice
{

    public static function buildTree($items)
    {
        $childs = array();

        foreach ($items as $item)
            $childs[$item->parent][] = $item;

        foreach ($items as $item) {
            if (isset($childs[$item->id]))
                $item->childs = $childs[$item->id];
        }

        return $childs;
    }

    public static function countryHelper($term, $mode = 'isoToCountryname')
    {
        if ($GLOBALS['stack']['countrylist'])
            return $GLOBALS['stack']['countrylist'][$term];

        $file = __DIR__ . '/../data/country/german-iso.csv';
        $file_handle = fopen($file, 'r');
        $stack = array();

        while (!feof($file_handle)) {
            $line = fgets($file_handle);
            $parts = preg_split('/\s+/', $line);
            $stack[$parts[0]] = $parts[1];
        }

        $GLOBALS['stack']['countrylist'] = $stack;
        return $stack[$term];
    }


    public static function countryHelperExtended($term, $mode = 'isoToCountryname', $list = 'de-world.json')
    {
        $file = __DIR__ . '/../data/country/' . $list;
        $data = json_decode(file_get_contents($file), 1);
        $stack = array();

        if ($mode == 'isoToCountryname') {
            foreach ($data as $oneCountry) {
                $stack[strtoupper($oneCountry['alpha2'])] = $oneCountry;
            }
        }

        if ($mode == 'iso3ToCountryname') {
            foreach ($data as $oneCountry) {
                $stack[strtoupper($oneCountry['alpha3'])] = $oneCountry;
            }
        }

        return $stack[$term];
    }
}
