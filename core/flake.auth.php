<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeAuth
{
    public static function createUser($email, $password, $firstname = '', $lastname = '', $position = '', $admin = false){
        $password = md5($password . $GLOBALS['encryptionkey']);
        
        if(F::tableExist('user'))
            $user = R::findOne('user', 'email = ?', [$email]);

        if(!$user){
            $p = R::dispense('user');
            $p->email = $email;
            $p->password = $password;
            $p->firstname = $firstname;
            $p->lastname = $lastname;
            $p->position = $position;
            $p->isAdmin = $admin ? 1 : 0;
            $p->public = md5(uniqid().$email.time());
            R::store($p);
        }
    }

    public static function checkUserLoginCredentials($email, $password)
    {
        $password = md5($password . $GLOBALS['encryptionkey']);
        $user = R::findOne('user', 'email = ? and password = ?', [$email, $password]);

        if ($user){
            $user->lastlogin = time();
            R::store($user);
            return $user->id;
        }
    }

    public static function login($email, $password)
    {
        $userId = self::checkUserLoginCredentials($email, $password);

        if ($userId) {
            flakeSession::setSession('time', time());
            flakeSession::setSession('current_user', $userId);
            return $userId;
        }
    }

    public static function logout()
    {
        session_destroy();
    }

    public static function getCurrentUser()
    {
        $userId = flakeSession::getSession('current_user');
        if ($userId) {

            if (isset($GLOBALS['current_user']))
                return $GLOBALS['current_user'];

            $user = R::findOne('user', 'id = ?', [$userId]);
            $GLOBALS['current_user'] = $user;

            return $user;
        }
    }

    public static function getCurrentUserId()
    {
        return flakeSession::getSession('current_user') ?: false;
    }

    public static function isAdmin(){
        $user = self::getCurrentUser();
        if($user->isAdmin == 1)
            return true;
    }
}
