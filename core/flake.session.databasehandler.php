<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeSessionDatabasehandler
{

    public function __construct()
    {
        $maxlifetime = 86400;
        $path = '/';
        $domain = '';
        $secure = true;
        $httponly = false;
        $samesite = 'None';

        if (PHP_VERSION_ID < 70300) {
            session_set_cookie_params($maxlifetime, $path . '; samesite=' . $samesite, $domain, $secure, $httponly);
        } else {
            @session_set_cookie_params(array(
                'lifetime' => $maxlifetime,
                'path' => $path,
                'domain' => $domain,
                'secure' => $secure,
                'httponly' => $httponly,
                'samesite' => $samesite
            ));
        }


        @session_set_save_handler(
            array($this, "_open"),
            array($this, "_close"),
            array($this, "_read"),
            array($this, "_write"),
            array($this, "_destroy"),
            array($this, "_gc")
        );

        @session_start();
    }

    public function _open()
    {
        return true;
    }

    public function _close()
    {
        return true;
    }

    public function _read($id)
    {

        $p = R::findOne('session', 'sessionkey = ?', [$id]);

        if ($p)
            return $p->data;

        return '';
    }

    public function _write($id, $data)
    {
        $p = R::findOne('session', 'sessionkey = ?', [$id]);

        if (!$p) {
            $p = R::dispense('session');
            $p->sessionkey = $id;
            $p->crdate = time();
        }

        $p->data = $data;
        $p->update = time();
        R::store($p);

        return true;
    }

    public function _destroy($id)
    {
        $p = R::findOne('session', 'sessionkey = ?', [$id]);

        if ($p->id > 0) {
            R::trash($p);

            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(
                    session_name(),
                    '',
                    time() - 42000,
                    $params["path"],
                    $params["domain"],
                    $params["secure"],
                    $params["httponly"]
                );
            }
        }

        return true;
    }

    public function _gc($max)
    {
        return true;
    }
}
