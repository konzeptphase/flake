<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeSetting
{

    public static function save($ident, $value)
    {
        $p = R::findOneOrDispense('setting', 'ident = ?', [$ident]);
        $p->ident = $ident;
        $p->value = $value;
        R::store($p);
    }

    public static function load($ident, $force = false)
    {
        if ($force) {
            $data = R::findOne('setting', 'ident = ?', [$ident]);
            return $data->value;
        } else {
            return $GLOBALS['setting'][$ident];
        }
    }

    public static function injectAll()
    {
        if(isset($GLOBALS['flake']['disableSettingsInjection']) && $GLOBALS['flake']['disableSettingsInjection'])
            return;

        foreach (R::findAll('setting') as $oneSetting)
            $GLOBALS['setting'][$oneSetting->ident] = $oneSetting->value;
    }
}
