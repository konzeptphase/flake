<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeAdministration
{

    public static function maintenance()
    {
        self::manageAction();

        $latte = new Latte\Engine;
        $latte->setTempDirectory('../../_cache_/');
        $parameters = flakeRouter::getBaseParameters();

        //layouting
        switch (flakeRouter::getRouterName()) {
            case 'entity':
                switch (flakeRouter::getRouterActionName()) {
                    case 'show':
                        $vParameters = $parameters;
                        $vParameters['entity'] = R::findOne('flakeentity', 'id = ?', [flakeRouter::getRouterActionValue()]);
                        $content = $latte->renderToString(dirname(__FILE__) . '/../backend/layout/entity-grid.latte', $vParameters);
                        break;
                    default:
                        $content = $latte->renderToString(dirname(__FILE__) . '/../backend/layout/entity.latte');
                        break;
                }
                break;
            case 'grid':
                switch (flakeRouter::getRouterActionName()) {
                    case 'show':
                        $vParameters = $parameters;
                        $vParameters['grid'] = R::findOne('flakegrid', 'id = ?', [flakeRouter::getRouterActionValue()]);
                        $content = $latte->renderToString(dirname(__FILE__) . '/../backend/layout/grid-attribute.latte', $vParameters);
                        break;
                    default:
                        flakeRouter::redirectHome();
                        break;
                }
                break;
            case 'unmount':
                flakeSession::deleteSession('maintenance');
                flakeRouter::redirectHome();
                break;
        }
        $parameters['content'] = $content;

        $html = $latte->renderToString(dirname(__FILE__) . '/../backend/template/index.latte', $parameters);
        $assetPath = dirname(__FILE__) . '/../vendor/tabler/tabler/dist/assets/';

        //attach css files
        $stylesheet = file_get_contents($assetPath . 'css/dashboard.css');
        $stylesheet .= file_get_contents($assetPath . 'plugins/charts-c3/plugin.css');
        $html = str_replace('###STYLESHEET###', '<style>' . $stylesheet . '</style>', $html);

        //attach js files
        $javascript = file_get_contents($assetPath . 'js/require.min.js');;
        $javascript .= file_get_contents($assetPath . 'js/plugins/charts-c3/plugin.js');;
        $javascript .= file_get_contents($assetPath . 'js/plugins/input-mask/plugin.js');;
        $javascript .= file_get_contents($assetPath . 'js/plugins/datatables/plugin.js');;
        $html = str_replace('###JAVASCRIPT###', '<script>' . $javascript . '</script>', $html);

        die($html);
    }

    private static function manageAction()
    {

        if ($_POST) {
            switch ($_POST['action']) {
                case 'create-entity':
                    $model = R::dispense('flakeentity');
                    $model->name = $_POST['name'];
                    $model->created = time();
                    R::store($model);

                    flakeRouter::redirect('entity');
                    break;
                case 'create-grid':
                    $model = R::dispense('flakegrid');
                    $model->entity = R::findOne('flakeentity', 'id = ?', [flakeRouter::getRouterActionValue()]);
                    $model->name = $_POST['name'];
                    $model->label = $_POST['label'];
                    $model->sorting = $_POST['sorting'] ?: 99;
                    $model->created = time();
                    R::store($model);

                    flakeRouter::redirect('entity/show/' . flakeRouter::getRouterActionValue());
                    break;
                case 'create-attribute':
                    $model = R::dispense('flakeattribute');
                    $model->grid = R::findOne('flakegrid', 'id = ?', [flakeRouter::getRouterActionValue()]);
                    $model->entity = $model->grid->entity;
                    $model->name = $_POST['name'];
                    $model->label = $_POST['label'];
                    $model->type = $_POST['attribute-type'];
                    $model->sorting = $_POST['sorting'] ?: 99;
                    $model->created = time();
                    R::store($model);

                    flakeRouter::redirect('grid/show/' . flakeRouter::getRouterActionValue());
                    break;
            }
        }

        switch (flakeRouter::getRouterName()) {
            case 'entity':
                switch (flakeRouter::getRouterActionName()) {
                    case 'delete':
                        R::trash('flakeentity', flakeRouter::getRouterActionValue());
                        flakeRouter::redirect('entity');
                        die();
                        break;
                }
                break;
            case 'grid':
                switch (flakeRouter::getRouterActionName()) {
                    case 'delete':
                        R::trash('flakegrid', flakeRouter::getRouterActionValue());
                        flakeRouter::redirect('entity');
                        die();
                        break;
                }
                break;
            case 'attribute':
                switch (flakeRouter::getRouterActionName()) {
                    case 'delete':
                        R::trash('flakeattribute', flakeRouter::getRouterActionValue());
                        flakeRouter::redirect('entity');
                        die();
                        break;
                }
                break;
        }
    }
}
