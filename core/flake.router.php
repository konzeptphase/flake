<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeRouter
{

    public static function splitUrl()
    {
        $baseurl = str_replace("http://", "", $GLOBALS['baseurl']);
        if ($_SERVER['HTTPS'] == "on")
            $baseurl = str_replace("https://", "", $GLOBALS['baseurl']);

        $urlpart = $_SERVER["SERVER_NAME"];
        $urlpart .= $_SERVER["REQUEST_URI"];

        $url = str_replace($baseurl, "", $urlpart);

        return $url;
    }

    public static function getRouterName()
    {
        $d = explode('/', self::splitUrl());
        $d[0] = strtok($d[0], '?');
        return $d[0];
    }

    public static function getRouterActionName()
    {
        $d = explode('/', self::splitUrl());
        $d = isset($d[1]) ? $d[1] : false;
        $d = strtok($d, '?');
        return $d;
    }

    public static function getRouterActionValue()
    {
        $d = explode('/', self::splitUrl());
        $d = isset($d[2]) ? $d[2] : false;
        $d = strtok($d, '?');
        return $d;
    }

    public static function getRouterActionOptionValue()
    {
        $d = explode('/', self::splitUrl());
        $d = isset($d[3]) ? $d[3] : false;
        $d = strtok($d, '?');
        return $d;
    }

    public static function getRouterValue($pathElement = 4)
    {
        $d = explode('/', self::splitUrl());
        $d = isset($d[$pathElement]) ? $d[$pathElement] : false;
        $d = strtok($d, '?');
        return $d;
    }

    public static function callRoute()
    {
        $r = self::getRouterName();

        if (!$r)
            $routeName = 'defaultRouter';
        else
            $routeName = self::getRouterName() . 'Router';

        //find the right router
        if (class_exists($routeName))
            return new $routeName;

        //instantiate home
        $class = "defaultRouter";
        return new $class;
    }

    public static function callStandalone($standaloneName)
    {
        //instantiate standalone
        $class = $standaloneName;
        return new $class;
    }

    public static function callVisual()
    {
        return '>>> this message should be not visible. please add the method "callVisual" to your router. <<<';
    }

    public static function renderTemplateObject($content, $template = 'index', $parameters = array(), $layoutmode = false, $path = false, $cacheFolder = false)
    {
        if(!is_array($parameters))
            $parameters = array();

        $latte = new Latte\Engine;
        $latte->setTempDirectory($cacheFolder ?? dirname(__FILE__).'../cache/');

        $parameters = array_merge($parameters, self::getBaseParameters());
        $parameters['content'] = $content;
        $html = $latte->renderToString(($path ?: '../designs/') . ($layoutmode ? 'layouts' : 'templates') . '/' . str_replace('.', '/', $template) . '.latte', $parameters);
        return $html;
    }

    public static function getBaseurl()
    {
        return $GLOBALS['baseurl'];
    }

    public static function redirect($path)
    {
        header('Location: ' . $GLOBALS['baseurl'] . $path);
    }

    public static function redirectHome()
    {
        header('Location: ' . $GLOBALS['baseurl']);
    }

    public static function getBaseParameters(){
        $parameters['version'] = $GLOBALS['version'];
        $parameters['title'] = $GLOBALS['title'];
        $parameters['masterhost'] = flakeNetservice::getHostname();
        $parameters['baseurl'] = $GLOBALS['baseurl'];
        $parameters['skinpath'] = ($parameters['skinurl'] ?: $parameters['baseurl']) . 'fileadmin/web';
        $parameters['mediapath'] = ($parameters['mediaurl'] ?: $parameters['baseurl']) . 'fileadmin/media';
        $parameters['user'] = flakeAuth::getCurrentUser();
        $parameters['router'] = flakeRouter::getRouterName();
        $parameters['actionvalue'] = flakeRouter::getRouterActionValue() ?: $_POST['actionvalue'];
        $parameters['actionoptionvalue'] = flakeRouter::getRouterActionOptionValue() ?: $_POST['actionoptionvalue'];
        $parameters['checksum'] = flakeSecurity::buildChecksum($parameters['actionvalue']);

        return $parameters;
    }
}
