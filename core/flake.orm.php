<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use RedBeanPHP\Finder;

class F extends R
{
	/**
	 * @var FFinder
	 */
	private static $finder;

	public static function loadJoined($beans, $type, $key = false, $overrideType = false, $sqlTemplate = 'SELECT %s.* FROM %s WHERE id IN (%s)')
	{
		if (!$key)
			return;

		if (!count($beans)) return array();
		$ids  = array();
		$key  = ($key ?: "{$type}_id");
		foreach ($beans as $bean) $ids[] = $bean->{$key};
		$result = self::findMulti($type, self::genSlots($beans, sprintf($sqlTemplate, $type, $type, '%s')), $ids, array(Finder::onmap($type, $beans)));

		foreach ($beans as &$oneBean) {
			$oneBean->{$overrideType ?: $type} = $result[$type][$oneBean->{$key}];
		}

		return $beans;
	}

	public static function tableExist($tablename, $tableschema = false)
	{
		if(!$tableschema)
			$tableschema = $GLOBALS['database_connection_string'][2];
			
		$data = R::getAll("SELECT COUNT(TABLE_NAME) as TABLECOUNTER FROM information_schema.TABLES WHERE TABLE_NAME = ? AND TABLE_SCHEMA = ?", [$tablename, $tableschema]);

		if ($data[0]['TABLECOUNTER'] == 0)
			return false;

		return true;
	}
}
