<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeSession
{

    private static function _initiateSession()
    {
        if(isset($GLOBALS['flake']['disableDbSessions']) && $GLOBALS['flake']['disableDbSessions'])
            return;
        
        if (session_status() > 0) {
            $databaseHandler = new flakeSessionDatabasehandler();
        }
    }

    private static function _closeSession()
    {
        session_write_close();
    }

    public static function setSession($key, $value)
    {
        self::_initiateSession();
        $_SESSION[$key] = $value;
        self::_closeSession();
    }

    public static function getSession($key)
    {
        self::_initiateSession();
        return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
        self::_closeSession();
    }

    public static function deleteSession($key)
    {
        self::_initiateSession();
        unset($_SESSION[$key]);
        self::_closeSession();
    }
}
