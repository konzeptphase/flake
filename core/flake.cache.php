<?php

/**
 * Copyright (c) Florian Bettzieche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class flakeCache
{

    public static function createCache($module, $type, $srcId, $data, $valid = 5)
    {
        $p = R::findOneOrDispense('cache', 'module = ? AND type = ? AND src_id = ?', [$module, $type, $srcId]);
        $p->module = $module;
        $p->type = $type;
        $p->srcId = $srcId;
        $p->data = json_encode($data);
        $p->crdate = time();
        $p->valid = time() + ($valid * 60);
        R::store($p);
    }

    public static function readCache($module, $type, $srcId)
    {
        R::exec('DELETE FROM cache WHERE valid < ' . time());
        $data[$type] = R::findOne('cache', 'module = ? AND type = ? AND src_id = ? ', [$module, $type, $srcId]);

        if ($data[$type])
            return json_decode($data[$type]->data, 1);
    }
}
